# Case Studies

NERSC staff along with Cray and Intel engineers have worked
with
[NESAP](https://www.nersc.gov/research-and-development/nesap/)
applications to optimize codes for the Cori architectures. Several of
these efforts are documenated as case studies.
