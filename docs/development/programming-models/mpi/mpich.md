# Cray MPICH

The default and preferred MPI implementation on Cray systems is
Cray-MPICH, and this is provided via the [Cray compiler
wrappers](../../../compilers/wrappers) and the `PrgEnv-*` modules
(whose suffix indicates which compiler the wrappers will use.
